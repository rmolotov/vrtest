using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
public enum AmmoType
{
    Pistol,
    Rifle,
    Shotgun
}

public class Pistol : XRSocketInteractor
{
    public AmmoType ammoType;
    Animator animator;
    AudioSource audioSource;
    new void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public void Shoot()
    {
        animator.SetTrigger("Fire");
        audioSource.Play();
    }

    public override bool CanSelect(XRBaseInteractable interactable)
    {
        return base.CanSelect(interactable) && interactable.GetComponent<Magazine>()?.ammoType == this.ammoType;
    }
}
