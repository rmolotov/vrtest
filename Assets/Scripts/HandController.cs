using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(ActionBasedController))]
public class HandController : MonoBehaviour
{
    Animator _animator;
    ActionBasedController _controller;
    
    void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _controller = GetComponent<ActionBasedController>();

        _controller.selectAction.action.performed += ctx => _animator.SetFloat("Grip", ctx.ReadValue<float>());
        _controller.selectAction.action.canceled += ctx => _animator.SetFloat("Grip", 0);

        _controller.activateAction.action.performed += ctx => _animator.SetFloat("Trigger", ctx.ReadValue<float>());
        _controller.activateAction.action.canceled += ctx => _animator.SetFloat("Trigger", 0);

    }
}
